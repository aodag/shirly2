class TestUser(object):

    def test_password(self):
        from shirly.models import User
        target = User(password="secret")

        assert not target.validate_password("secretx")
        assert target.validate_password("secret")

