import webtest


def _get_app():
    settings = {
        "authtkt.secret": "secret",
        "sqlalchemy.url": "sqlite:///",
        "shirly.create_model": True,
    }
    from shirly import main
    app = main({}, **settings)
    app = webtest.TestApp(app)
    return app


class TestHello(object):

    def test_hello(self):
        app = _get_app()
        res = app.get("/")
        assert res.text == "Hello"


class TestLogin(object):
    def test_login_invalid_params(self):
        app = _get_app()
        res = app.post("/login", status=400)
        assert res.json == {'password': 'Required', 'username': 'Required'}

    def test_login_invalid_user(self):
        app = _get_app()
        res = app.post("/login", status=400, params={"username": "dummy", "password": "secret"})
        assert res.json == {"username": "'dummy' not found"}

    def test_login_invalid_password(self):
        app = _get_app()
        from shirly.models import DBSession, User
        DBSession.add(User(username="dummy", password="xsecret"))
        import transaction
        transaction.commit()
        res = app.post("/login", status=400, params={"username": "dummy", "password": "secret"})

        assert res.json == {"password": "invalid"}

    def test_login_correct(self):
        app = _get_app()
        from shirly.models import DBSession, User
        DBSession.add(User(username="dummy", password="secret"))
        import transaction
        transaction.commit()
        res = app.post("/login", params={"username": "dummy", "password": "secret"})

        assert res.json == {'username': 'dummy'}
