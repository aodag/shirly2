import hashlib
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    Unicode,
)
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)
from sqlalchemy.ext.declarative import declarative_base
from zope.sqlalchemy import ZopeTransactionExtension


Base = declarative_base()
DBSession = scoped_session(
    sessionmaker(extension=ZopeTransactionExtension()))


def init(engine, create_model=False):
    DBSession.remove()
    DBSession.configure(bind=engine)
    if create_model:
        Base.metadata.create_all(bind=engine)


class User(Base):
    __tablename__ = 'users'
    query = DBSession.query_property()
    id = Column(Integer, primary_key=True)
    username = Column(Unicode(255), unique=True)
    password_digest = Column(String(255))

    def digest_password(self, password):
        return hashlib.sha1(password.encode('utf-8')).hexdigest()

    def set_password(self, password):
        self.password_digest = self.digest_password(password)

    password = property(fset=set_password)

    def validate_password(self, password):
        return self.password_digest == self.digest_password(password)
