import logging
import json
import colander as c
from pyramid.view import view_config
from pyramid import security
from pyramid.httpexceptions import HTTPBadRequest, HTTPFound
from .models import User


logger = logging.getLogger(__name__)
logger.debug("loaded")


class LoginSchema(c.Schema):
    username = c.SchemaNode(c.String())
    password = c.SchemaNode(c.String())


@view_config(route_name="login", renderer="json")
def login(request):
    try:
        schema = LoginSchema()
        params = schema.deserialize(request.params)
    except c.Invalid as e:
        request.response.status = 400
        return e.asdict()

    username = params["username"]
    password = params["password"]
    user = User.query.filter(User.username==username).first()
    if user is None:
        request.response.status = 400
        return {"username": "'{0}' not found".format(username)}

    if not user.validate_password(password):
        request.response.status = 400
        return {"password": "invalid"}

    headers = security.remember(request, user.username)
    
    return {'username': user.username}
