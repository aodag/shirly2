import logging
from sqlalchemy import engine_from_config
from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from . import models


logger = logging.getLogger(__name__)

def hello(request):
    logger.info("hello {0}".format(request.url))
    request.response.text = "Hello"
    return request.response


def includeme(config):
    config.add_route("login", "login")
    config.scan(".views")


def main(global_conf, **settings):
    engine = engine_from_config(settings)
    create_model = settings.get('shirly.create_model', False)
    models.init(engine, create_model=create_model)
    secret = settings["authtkt.secret"]
    config = Configurator(settings=settings)
    config.include("pyramid_tm")
    config.include(".")
    authentication_policy = AuthTktAuthenticationPolicy(secret)
    config.set_authentication_policy(authentication_policy)
    config.set_authorization_policy(ACLAuthorizationPolicy())
    config.add_view(hello)
    return config.make_wsgi_app()
