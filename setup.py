from setuptools import setup


requires = [
    "pyramid",
]


setup(name="shirly2",
      packages=["shirly"],
      install_requires=requires,
)
